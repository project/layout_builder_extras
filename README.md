# Layout Builder Extras

This module intends to provide some extra functionality and options that haven't yet made it to core Layout Builder.

## Included functionality
* A hook_preprocess_entity() inserts a default langcode if none exists for Paragraph entities. This allows Layout Builder to properly save Inline Custom Blocks that contain a Paragraph reference.

## Included modules
### Layout Builder Tray Resize
This module will give some additional display settings and configuration for the off-canvas dialog that opens up on the left hand side when using Layout Builder.

